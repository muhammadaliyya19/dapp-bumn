import React, { Component } from "react";
// import Rekrutmen from "../contracts/Rekrutmen.json";
import Rekrutmen from "../contracts/Rekrutmen.json";
import getWeb3 from "../getWeb3";
import axios from 'axios';

import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';

import { FormGroup, FormControl, Button } from 'react-bootstrap';
import Sidebar from '../layouts/Sidebar';  
import SidebarAdmin from '../layouts/SidebarAdmin';  
import Header from '../layouts/Header'  
import Footer from '../layouts/Footer'  
class ApplyRekrutmenOri extends Component {
  constructor(props) {
    super(props)

    this.state = {
      RekrutmenInstance: undefined,
      account: null,
      web3: null,
      nama: '',
      nik: '',
      ipk: 0,
      ttl: '',
      alamat: '',
      rekrutmen_id: null,
      terdaftar: false,
      is_verified: 0,
      isOwner: false,
      selectedFile: null,
    }
  }

  updateIPK = event => {
    this.setState({ ipk: event.target.value });
  }

  onFileChange = (event) => {
    this.setState({ selectedFile: event.target.files[0] });
  };

  onFileUpload = () => {
    const formData = new FormData();
    formData.append("myFile", this.state.selectedFile);

    console.log(this.state.selectedFile);
    axios.post("http://localhost:3000/", formData, {
      headers: {
        "content-type": "multipart/form-data",
      },
    }); //I need to change this line
  };

  fileData = () => {
    if (this.state.selectedFile) {
      return (
        <div>
          <h2>File Details:</h2>
          <p>File Name: {this.state.selectedFile.name}</p>
          <p>File Type: {this.state.selectedFile.type}</p>
          <p>
            Last Modified:{' '}
            {this.state.selectedFile.lastModifiedDate.toDateString()}
          </p>
        </div>
      );
    } else {
      return (
        <div>
          <br />
          <h4>Choose before Pressing the Upload button</h4>
        </div>
      );
    }
  };

  lamarRekrutmen = async () => {
    await this.state.RekrutmenInstance.methods.mendaftarRekrutmen(this.state.rekrutmen_id).send({ from: this.state.account, gas: 1000000 });
    window.location.href= '/MyRegist';
  }

  componentDidMount = async () => {
    // FOR REFRESHING PAGE ONLY ONCE -
    if (!window.location.hash) {
      window.location = window.location + '#loaded';
      window.location.reload();
    }
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Rekrutmen.networks[networkId];
      const instance = new web3.eth.Contract(
        Rekrutmen.abi,
        deployedNetwork && deployedNetwork.address,
      );
      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.

      // this.setState({ web3, accounts, contract: instance }, this.runExample);
      this.setState({ RekrutmenInstance: instance, web3: web3, account: accounts[0] });

      // let jumlahPemilih = await this.state.RekrutmenInstance.methods.mendapatkanJumlahPemilih().call();
      
      // GET PARAMS
      // Get params
      const this_params = this.props.match.params;
      this.setState({ rekrutmen_id: this_params.id_rekrutmen });

      // Cek status pendaftaran
      let terdaftar = await this.state.RekrutmenInstance.methods.cek_regist_exist(this.state.account, this.state.rekrutmen_id).call();
      this.setState({ terdaftar: terdaftar });

      let dataDiriku = await this.state.RekrutmenInstance.methods.list_calon_pelamar(this.state.account).call();        
      if (dataDiriku[1] !== "") {
        // Kalau dataDiriKu ada maka dilakukan set-state
        this.setState({ 
            nama        : dataDiriku[1],
            nik         : dataDiriku[2],
            // ipk         : dataDiriku[3],
            ttl         : dataDiriku[3],
            alamat      : dataDiriku[4],
        });
      }

      const owner = await this.state.RekrutmenInstance.methods.Admin().call();
      if (this.state.account === owner) {
        this.setState({ isOwner: true });
      }
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  render() {
    if (!this.state.web3) {
      return (
        <div className="CandidateDetails">
          <div className="CandidateDetails-title">
            <h1>
              Loading Web3, accounts, and contract..
            </h1>
          </div>
          {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
        </div>
      );
    }

    if (this.state.terdaftar) {
      return (
        // <div className="">
        //   <div className="CandidateDetails">
        //     {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
        //     <div className="CandidateDetails-title">
              <h1>
                FORMULIR PENDAFTARAN REKRUTMEN - Tahap 1
              </h1>
        //     </div>
        //   </div>
        //   <br></br>
        //   <div className="row">
        //   <div className="col-lg-1"></div>
        //   <div className="col-lg-10">                     
        //     <h1 className="text-center">
        //       Anda Sudah Mendaftar Rekrutmen Ini.
        //     </h1>         
        //   </div>
        //   <div className="col-lg-1"></div>                          
        //   </div>
        // </div>
      );
    }else{
      return (
        <div>
          <div className="CandidateDetails">
                  {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
                      <div className="CandidateDetails-title">
                          <h1>
                              FORMULIR PENDAFTARAN REKRUTMEN - Tahap 1
                          </h1>
                      </div>
                  </div>
                  <br></br>
                  <div className="row">
                  <div className="col-lg-1"></div>
                  <div className="col-lg-10">
          
                  <FormGroup>
                    <label>Nama</label>
                    <div className="form-input">
                      <FormControl
                        input='text'
                        readOnly
                        value={this.state.nama}
                      />
                    </div>
                  </FormGroup>          
                  <FormGroup>
                    <label>NIK</label>
                    <div className="form-input">
                      <FormControl
                        input='text'
                        value={this.state.nik}
                        readOnly
                      />
                    </div>
                  </FormGroup>
                  <FormGroup>
                    <label>IPK</label>
                    <div className="row">
                      <div className="col-md-6 col-6">
                        <div className="form-input">
                          <FormControl
                            input='number'
                            value={this.state.ipk}
                            onChange={this.updateIPK}
                          />
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <input type="file" onChange={this.onFileChange} />
                        <button onClick={this.onFileUpload}>Upload!</button>
                        {this.fileData()}
                      </div>
                    </div>
                  </FormGroup>
                  <FormGroup>
                    <label>Tempat / Tanggal Lahir</label>
                    <div className="form-input">
                      <FormControl
                        input='text'
                        value={this.state.ttl}
                        readOnly
                      />
                    </div>
                  </FormGroup>
                  <FormGroup>
                    <label>Alamat</label>
                    <div className="form-input">
                      <FormControl
                        input='text'
                        value={this.state.alamat}
                        readOnly
                      />
                    </div>
                  </FormGroup>  
                  <FormGroup>
                    <label>Pilih Rekrutmen (Input Kode Rekrutmen)</label>
                    <div className="form-input">
                      <FormControl
                        input='text'
                        readOnly
                        value={this.state.rekrutmen_id}
                        onChange={this.updateRekrutmenId}
                      />
                    </div>
                  </FormGroup>

                  <Button onClick={this.lamarRekrutmen} className="button-vote">
                    Daftar
                  </Button>
          </div>
          <div className="col-lg-1"></div>                
          
          </div>
        </div>
          
      );
    }
  }
}

export default ApplyRekrutmenOri;
