import React, { Component } from "react";
import Rekrutmen from "../contracts/Rekrutmen.json";
import getWeb3 from "../getWeb3";
import { Form, FormGroup, FormControl, Button, Modal } from 'react-bootstrap';
import '../index.css';
import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';
import emailjs from '@emailjs/browser';
// import { Link, useParams, useRoute } from 'react-router-dom';
import Sidebar from '../layouts/Sidebar';  
import SidebarAdmin from '../layouts/SidebarAdmin';  
import Header from '../layouts/Header'  
import Footer from '../layouts/Footer'  

class VerifyRegist extends Component {
  constructor(props) {
    super(props)

    this.state = {
      RekrutmenInstance: undefined,
      account: null,
      web3: null,
      ThisRec : null,
      RekrutmenId : null,
      SemuaRegist: null,
      show: false,
      id_pelamar: "",
      nama: "",
      nik: "",
      email: "",
      judul: "",
      is_verified: 0,
      alasan: "",
      isOwner: false
    }
//inisiasi fungsi menampilkan pop up
    this.handleClose = this.handleClose.bind(this);
    this.handleShowModal = this.handleShowModal.bind(this);
  }

  handleShowModal(id_pelamar){
    let nama = this.state.SemuaRegist[id_pelamar].nama;
    let nik = this.state.SemuaRegist[id_pelamar].nik;
    let email = this.state.SemuaRegist[id_pelamar].email;
    let judul = this.state.SemuaRegist[id_pelamar].judul;
    this.setState({
        show: true, id_pelamar: id_pelamar, nama: nama, nik: nik, email:email, judul:judul
    })
  }

  handleClose(){
      this.setState({
          show:false
      })
  }

  updateStatus = event => {
    this.setState({ is_verified: event.target.value });
  }  

  updateAlasan  = event => {
    this.setState({ alasan: event.target.value });
  }

  verifikasi = async (id_pelamar) => {
    let alasan = this.state.alasan;
    let is_verified = this.state.is_verified;
    console.log(alasan);
    console.log(is_verified);    
    // console.log('OTW send email');
    // this.sendNotifEmail();
    // console.log('Beres send email');
    // await this.state.RekrutmenInstance.methods.verify_pelamar(id_pelamar, is_verified, alasan).send({ from: this.state.account, gas: 1000000 });    
    await this.state.RekrutmenInstance.methods.verify_pelamar(id_pelamar, is_verified, alasan).send({ from: this.state.account, gas: 1000000 });    

    // Kirim email
    
    window.location.reload(false);
    // // window.location.href= '/ListRekrutmen';
    
  }

  sendNotifEmail = event => {
    var desc = "";
    switch (this.state.is_verified) {
      case '1':
        desc = "Selamat anda lolos tahap satu !";
        break;
    case '2':
        desc = "Maaf anda belum lolos tahap satu !";        
        break;
    case '3':
        desc = "Selamat anda lolos dan diterima di rekrutmen kami ! Cek lebih lanjut informasi ini di DAPP Rekrutmen anda.";        
        break;
    case '4':
        desc = "Maaf anda belum lolos rekrutmen.";        
        break;
      default:
        break;
    }
    let data_email = {
        judul: this.state.judul,
        message: desc,
        from_name: 'DAPP REKRUTMEN',
        to_name:'Sobat DAPP Rekrutmen',
        to_email:this.state.email,
        reply_to:'muhammadasaadilhaqisya@gmail.com',
    };
    emailjs.send('service_pfikdd8', 'template_k4jubck', data_email, 'BEt5phAuTwtxz4Nhw');
  }

  componentDidMount = async () => {
    // FOR REFRESHING PAGE ONLY ONCE -
    if (!window.location.hash) {
      window.location = window.location + '#loaded';
      window.location.reload();
    }

    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Rekrutmen.networks[networkId];
      const instance = new web3.eth.Contract(
        Rekrutmen.abi,
        deployedNetwork && deployedNetwork.address,
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.

      // this.setState({ web3, accounts, contract: instance }, this.runExample);
      this.setState({ RekrutmenInstance: instance, web3: web3, account: accounts[0] });

      // Get params
    const this_params = this.props.match.params;
    this.setState({ RekrutmenId: this_params.id_rekrutmen });
      
    let jumlah_rekrutmen = await this.state.RekrutmenInstance.methods.getSumRekrutmen().call();
    let daftar_rekrutmen = [];
    for (let i = 0; i < jumlah_rekrutmen; i++) {
        let rec = await this.state.RekrutmenInstance.methods.List_Rekrutmen(i).call();
        daftar_rekrutmen.push(rec);

        if(rec.id == this_params.id_rekrutmen){
          this.setState({ ThisRec: rec });
        }
    }
    // console.log(this.state.ThisRec)
      
    //   Populasi pendaftaran all
    let jumlahPendaftar = await this.state.RekrutmenInstance.methods.getSumAllPendaftar().call();
    let ListPendaftaran = [];
      for (let i = 0; i < jumlahPendaftar; i++) {
        let detailPendaftar = await this.state.RekrutmenInstance.methods.list_pelamar(i).call();                
        console.log(detailPendaftar)
        
        let biodataPendaftar = await this.state.RekrutmenInstance.methods.list_calon_pelamar(detailPendaftar[0]).call();                
        console.log(biodataPendaftar)
        
        // Set biodata p[endaftar]
        detailPendaftar.nama = biodataPendaftar.nama;
        detailPendaftar.nik = biodataPendaftar.nik;
        detailPendaftar.alamat = biodataPendaftar.alamat;
        detailPendaftar.email = biodataPendaftar.email;
        // detailPendaftar.ipk = biodataPendaftar.ipk;

        // Set judul rekrutmen
        detailPendaftar.namaRekrutmen = daftar_rekrutmen[detailPendaftar.rekrutmen_id].judul;        
        
        ListPendaftaran.push(detailPendaftar);        
      }
      this.setState({ SemuaRegist: ListPendaftaran });      
      
      const owner = await this.state.RekrutmenInstance.methods.Admin().call();
      if (this.state.account === owner) {
        this.setState({ isOwner: true });
      }
      
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };  

  render() {
    // let id;          
    let idrec = this.state.RekrutmenId;
    let allRegist = [];    
    let allRegistTable = [];    
    if (this.state.SemuaRegist) {
      console.log(this.state)
      if (this.state.SemuaRegist.length > 0) {
        for (let j = 0; j < this.state.SemuaRegist.length; j++) {
          // console.log("Cek Data",this.state.SemuaRegist[j]);
          if(this.state.SemuaRegist[j].rekrutmen_id == idrec){
            let statusVerif = [];
            let buttonVerif = [];
            let alasanLolos = [];
            switch (this.state.SemuaRegist[j].is_verified) {
              case '1':
                buttonVerif.push("");
                statusVerif.push(<span className="text-success">Lolos Verifikasi Tahap Satu</span>);
                alasanLolos.push("Alasan Kelolosan : ");
                alasanLolos.push(this.state.SemuaRegist[j].alasan);              
                buttonVerif.push(<Button className="btn-sm" variant="primary" onClick={() => this.handleShowModal(j)}>Verifikasi Tahap Dua</Button>);              
                break;
              case '2':
                buttonVerif.push(<Button className="btn-sm" variant="danger">#</Button>);
                statusVerif.push(<span className="text-danger">Tidak Lolos Verifikasi</span>);
                alasanLolos.push("Alasan Tidak Lolos : ");
                alasanLolos.push(this.state.SemuaRegist[j].alasan);              
                break;
              case '3':
                buttonVerif.push(<Button className="btn-sm" variant="success">#</Button>);
                statusVerif.push(<span className="text-success">Lolos Rekrutmen</span>);                
                break;
              case '4':
                buttonVerif.push(<Button className="btn-sm" variant="danger">#</Button>);
                statusVerif.push(<span className="text-danger">Tidak Lolos Rekrutmen</span>);                
                break;
              default:
              buttonVerif.push(<Button className="btn-sm" variant="primary" onClick={() => this.handleShowModal(j)}>Verifikasi</Button>);              
              statusVerif.push(<span className="text-primary">Belum Diverifikasi</span>);
              break;
            }
            // Old all regist
            // allRegist.push(
            //   <div className="candidate">
            //     <div className="candidateName">{this.state.SemuaRegist[j].nama} || {statusVerif}</div>
            //     <div className="CandidateDetails">            
            //       <div>NIK : {this.state.SemuaRegist[j].nik}</div>
            //       <div>Alamat : {this.state.SemuaRegist[j].alamat}</div>
            //       <div>ID Rekrutmen : {this.state.SemuaRegist[j].rekrutmen_id}</div>
            //       <div>Nama Rekrutmen : {this.state.SemuaRegist[j].namaRekrutmen}</div>
            //       <div>{alasanLolos}</div>
            //     </div>
            //     <div className="CandidateDetails">            
            //       {buttonVerif}
            //       {
            //         <Modal show={this.state.show} onHide={this.handleClose} backdrop="static">
            //         <Modal.Header closeButton>
            //             <Modal.Title>Verifikasi Pendaftaran</Modal.Title>
            //         </Modal.Header>
            //         <Modal.Body>
            //             <FormGroup>
            //               <label>Nama</label>
            //               <div className="form-input">
            //                 <FormControl
            //                   input='text'
            //                   disabled='true'
            //                   value={this.state.nama}
            //                 />
            //               </div>
            //             </FormGroup>          
            //             <FormGroup>
            //               <label>NIK</label>
            //               <div className="form-input">
            //                 <FormControl
            //                   input='text'
            //                   disabled='true'
            //                   value={this.state.nik}
            //                 />
            //               </div>
            //             </FormGroup>
            //             <FormGroup>
            //               <label>E-Mail</label>
            //               <div className="form-input">
            //                 <FormControl
            //                   input='text'
            //                   disabled='true'
            //                   value={this.state.email}
            //                 />
            //               </div>
            //             </FormGroup>
            //             <FormGroup>
            //             <label>Status Verifikasi</label>
                          
            //               <Form.Control 
            //                 as="select"
            //                 className="form-control"
            //                 custom
            //                 onChange={this.updateStatus.bind(this)}
            //               >
            //                 <option value="0"> -- Pilih Status Verifikasi -- </option>
            //                 <option value="1"> Lolos Verifikasi </option>
            //                 <option value="2"> Tidak Lolos Verifikasi </option>     
            //                 <option value="3"> Lolos dan Diterima </option>                    
            //                 <option value="4"> Tidak Lolos </option>                                   
            //               </Form.Control>
            //             </FormGroup>
            //             <FormGroup>
            //               <label>Alasan Status Verifikasi</label>
            //               <div className="form-input">
            //                 <FormControl
            //                   input='text'
            //                   onChange={this.updateAlasan.bind(this)}
            //                 />
            //               </div>
            //             </FormGroup>                
            //         </Modal.Body>
            //         <Modal.Footer>
            //             <Button variant="primary" onClick={() => { this.verifikasi(this.state.id_pelamar) }}>
            //                 Simpan
            //             </Button>
            //             <Button variant="danger" onClick={this.handleClose}>
            //                 Batal
            //             </Button>
            //         </Modal.Footer>
            //     </Modal>
            //       }
            //     </div>
            //     <br></br>
            //   </div>
            // );
            // new allregist - using table
            allRegistTable.push(
              <tr>
                  {/* <div>NIK : {this.state.SemuaRegist[j].nik}</div>
                  <div>Alamat : {this.state.SemuaRegist[j].alamat}</div>
                  <div>ID Rekrutmen : {this.state.SemuaRegist[j].rekrutmen_id}</div>
                  <div>Nama Rekrutmen : {this.state.SemuaRegist[j].namaRekrutmen}</div>
                  <div>{alasanLolos}</div> */}
                <td>{this.state.SemuaRegist[j].nik}</td>
                <td>{this.state.SemuaRegist[j].nama}</td>
                <td>{this.state.SemuaRegist[j].alamat}</td>
                <td>{this.state.SemuaRegist[j].rekrutmen_id}</td>
                <td>{statusVerif}</td>
                <td>{buttonVerif}</td>
                {
                    <Modal show={this.state.show} onHide={this.handleClose} backdrop="static">
                    <Modal.Header closeButton>
                        <Modal.Title>Verifikasi Pendaftaran</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup>
                          <label>Nama</label>
                          <div className="form-input">
                            <FormControl
                              input='text'
                              disabled='true'
                              value={this.state.nama}
                            />
                          </div>
                        </FormGroup>          
                        <FormGroup>
                          <label>NIK</label>
                          <div className="form-input">
                            <FormControl
                              input='text'
                              disabled='true'
                              value={this.state.nik}
                            />
                          </div>
                        </FormGroup>
                        <FormGroup>
                          <label>E-Mail</label>
                          <div className="form-input">
                            <FormControl
                              input='text'
                              disabled='true'
                              value={this.state.email}
                            />
                          </div>
                        </FormGroup>
                        <FormGroup>
                        <label>Status Verifikasi</label>
                          
                          <Form.Control 
                            as="select"
                            className="form-control"
                            custom
                            onChange={this.updateStatus.bind(this)}
                          >
                            <option value="0"> -- Pilih Status Verifikasi -- </option>
                            <option value="1"> Lolos Verifikasi </option>
                            <option value="2"> Tidak Lolos Verifikasi </option>     
                            <option value="3"> Lolos dan Diterima </option>                    
                            <option value="4"> Tidak Lolos </option>                                   
                          </Form.Control>
                        </FormGroup>
                        <FormGroup>
                          <label>Alasan Status Verifikasi</label>
                          <div className="form-input">
                            <FormControl
                              input='text'
                              onChange={this.updateAlasan.bind(this)}
                            />
                          </div>
                        </FormGroup>                
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => { this.verifikasi(this.state.id_pelamar) }}>
                            Simpan
                        </Button>
                        <Button variant="danger" onClick={this.handleClose}>
                            Batal
                        </Button>
                    </Modal.Footer>
                </Modal>
                  }
              </tr>
            )
          }        
        }    
      }else{
        allRegist.push(
          <div className="text-center">
            <h1> BELUM ADA DATA PENDAFTAR </h1>
          </div>
          );
      }
    }

    if (!this.state.web3) {
      return (
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-12 pl-4 text-center">
                  <h1>
                    Loading Web3, accounts, and contract..
                  </h1>
                </div>
              </div>
          </div>
        </div>  
      </div> 
      );
    }

    if (!this.state.isOwner) {
      return (        
        <div id="wrapper">
        {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
            <Header />
            <div className="row">
              <div className="col-lg-12 pl-4 text-center">
                <h1>
                  HANYA ADMIN YANG DAPAT MENGAKSES
                </h1>
              </div>
            </div>
        </div>
      </div>  
    </div> 
      );
    }

    return (
      // OLD LAYOUT
      // <div>
      //   <div className="CandidateDetails">
      //   {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
      //     <div className="CandidateDetails-title">
      //       <h1>
      //         VERIFIKASI PENDAFTARAN {/*this.state.SemuaRegist[this.state.RekrutmenId].namaRekrutmen*/}
      //       </h1>
      //     </div>
      //   </div>

      //   <div>
      //     {allRegist}
      //   </div>
      // </div>

      // NEW LAYOUT
      <div>
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-6 pl-4">
                  <h4>Verifikasi Pendaftaran Rekrutmen { this.state.ThisRec[1] }</h4>
                </div>
              </div>
              <div className="card m-3">
                <div className="card-body" style={{ maxHeight: '500px', overflowY: 'scroll' }}>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">NIK</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Kode Rekrutmen</th>
                        <th scope="col">Status</th>
                        <th scope="col">Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      {allRegistTable}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </div>  
      </div> 
    );
  }
}

export default VerifyRegist;
