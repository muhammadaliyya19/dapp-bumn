import React, { Component } from "react";
// import Beasiswa from "../contracts/Beasiswa.json";
import Rekrutmen from "../contracts/Rekrutmen.json";
import getWeb3 from "../getWeb3";

import '../index.css';

import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';
import { Link } from 'react-router-dom';
import Sidebar from '../layouts/Sidebar';  
import SidebarAdmin from '../layouts/SidebarAdmin';  
import Header from '../layouts/Header'  
import Footer from '../layouts/Footer'  

class ListPelamar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // EvotingInstance: undefined,
      RekrutmenInstance: undefined,
      account: null,
      web3: null,
      jumlah_pelamar: 0,
      listPelamar: [],
      loaded: false,
      isOwner: false,
    }
  }

  // getCandidates = async () => {
  //   let result = await this.state.EvotingInstance.methods.getCandidates().call();

  //   this.setState({ candidates : result });
  //   for(let i =0; i <result.length ; i++)


  // }

  componentDidMount = async () => {

    // FOR REFRESHING PAGE ONLY ONCE -
    if (!window.location.hash) {
      window.location = window.location + '#loaded';
      window.location.reload();
    }

    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Rekrutmen.networks[networkId];
      const instance = new web3.eth.Contract(
        Rekrutmen.abi,
        deployedNetwork && deployedNetwork.address,
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.

      // this.setState({ web3, accounts, contract: instance }, this.runExample);
        this.setState({ RekrutmenInstance: instance, web3: web3, account: accounts[0] });

        let jumlah_pelamar = await this.state.RekrutmenInstance.methods.getSumAllPelamar().call();
        this.setState({ jumlah_pelamar: jumlah_pelamar });

        let all_pelamar = [];
        // for (let j = 0; j < 10; j++) {
        for (let i = 0; i < jumlah_pelamar; i++) {
            let pel = await this.state.RekrutmenInstance.methods.list_pelamar_admin(i).call();
            all_pelamar.push(pel);
        }
        // }

        // daftar_pelamar = await this.state.RekrutmenInstance.methods.getAllPelamar().call();
        console.log('all Pelamar');
        console.log(all_pelamar);

        this.setState({ listPelamar: all_pelamar });

      const owner = await this.state.RekrutmenInstance.methods.Admin().call();
      if (this.state.account === owner) {
        this.setState({ isOwner: true });
      }

    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };


  render() {
    let daftarPelamar;
    if (this.state.listPelamar) {
        if (this.state.listPelamar.length > 0) {
            daftarPelamar = this.state.listPelamar.map((Data_Pelamar) => {
                return (
                    <tr>
                      <td>{Data_Pelamar.nik}</td>
                      <td>{Data_Pelamar.nama}</td>
                      <td>{Data_Pelamar.ttl}</td>
                      <td>{Data_Pelamar.alamat}</td>
                      <td>{Data_Pelamar.email}</td>
                    </tr>
                );
            });
        }   
    }  

    if (!this.state.web3) {
      return (
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-12 pl-4">
                  <h1>
                    Loading Web3, accounts, and contract..
                  </h1>
                </div>
              </div>
          </div>
        </div>  
      </div> 
      );
    }

    return (
      // <div className="CandidateDetails">
      //   {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
      //   <div className="CandidateDetails-title">
      //     <h1>
      //       {" LIST PELAMAR TERDAFTAR di SISTEM "}
      //     </h1>
      //   </div>


      //   <div className="section-title">
      //     <h3>Total Jumlah Pelamar : <span>{this.state.jumlah_pelamar}</span></h3>
      //   </div>
      //   <div>
      //     {daftarPelamar}
      //   </div>
      // </div>
      <div>
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-6 pl-4">
                  <h4>Data Pelamar</h4>
                </div>
                <div className="col-lg-6 text-right pr-4">
                    <h4>Total Jumlah Pelamar : <span>{this.state.jumlah_pelamar}</span></h4>
                </div>
              </div>
              <div className="card m-3">
                <div className="card-body" style={{ maxHeight: '500px', overflowY: 'scroll' }}>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">NIK</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Tempat / Tanggal Lahir</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">E-Mail</th>
                      </tr>
                    </thead>
                    <tbody>
                      {daftarPelamar}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </div>  
      </div> 
    );
  }
}

export default ListPelamar;
