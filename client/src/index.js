import React from 'react';
import ReactDOM from 'react-dom';

// import './index.css';
import './assets/vendor/aos/aos.css';
// import './assets/vendor/bootstrap/css/bootstrap.min.css';
// import './assets/vendor/bootstrap-icons/bootstrap-icons.css';
// import './assets/vendor/boxicons/css/boxicons.min.css';
// import './assets/vendor/glightbox/css/glightbox.min.css';
// import './assets/vendor/swiper/swiper-bundle.min.css';

import './assets/css/style.css';
// import 'bootstrap/dist/css/bootstrap.min.css';  

import Home from './components/Home';
import HomeSB from './components/HomeSB';
import ListRekrutmen from './components/ListRekrutmen';
import AddRekrutmen from './components/AddRekrutmen';
import MyRegist from './components/MyRegist';
import DataDiri from './components/DataDiri';
import ListPelamar from './components/ListPelamar';
import ApplyRekrutmen from './components/ApplyRekrutmen';
import VerifyRegist from './components/VerifyRegist';

// import * as serviceWorker from './serviceWorker';

import { Router, Switch, Route } from 'react-router-dom';
import history from './history';

// ReactDOM.render(<App />, document.getElementById('root'));

ReactDOM.render(
    <Router history={history}>
        <Switch>
            {/* 
                route & component
                - home
                - list rekrutmen
                - add rekrutmen
                - apply rekrutmen
                - show list pendaftar by rekrutmen
                - verify apply rekrutmen
                - pendaftaran saya
                - list pendaftar
                - data diri
            */}
            <Route exact path='/' component={Home} />
            <Route exact path='/sb' component={HomeSB} />
            <Route path='/ListRekrutmen' component={ListRekrutmen} />
            <Route path='/AddRekrutmen' component={AddRekrutmen} />
            <Route path='/MyRegist' component={MyRegist} />
            <Route path='/DataDiri' component={DataDiri} />
            <Route path='/ListPelamar' component={ListPelamar} />
            <Route path='/RegistRekrutmen/:id_rekrutmen' component={ApplyRekrutmen} />
            <Route path='/VerifyRegist/:id_rekrutmen' component={VerifyRegist} />
        </Switch>
    </Router>,
    document.getElementById('root')
);

// serviceWorker.unregister();
